# OpenML dataset: FacultySalaries

https://www.openml.org/d/1096

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Datasets of Data And Story Library, project illustrating use of basic statistic methods, converted to arff format by Hakan Kjellerstrand.
Source: TunedIT: http://tunedit.org/repo/DASL

DASL file http://lib.stat.cmu.edu/DASL/Datafiles/FacultySalaries.html

Professors' Pay

Reference:   "Faculty Compensation and Benefits Committee." (1993, April).  Ohio State University
Authorization:   free use
Description:   Average salaries for professors at the top 50 universities of the Association of American Universities.  Salaries of full, associate and assistant professors at these universities are provided.  1992-1993 data from Clark University are not available; 1991-1992 salaries were substituted.
* Note that the "Big Ten" has 11 schools in it (since Penn State joined the original 10 schools)
Number of cases:   50
Variable Names:

University:   Name of the university
CIC.institutions:   1 if in the Committee on Institutional Cooperation (the Big Ten* plus University of Chicago); 0 if the university is not in the CIC
average.salary:   Average salary for all professors at the university assuming that the proportions of faculty in each rank are the same as those proportions at the Ohio State University
full.prof.salary:   Average salary for full professors at the university in 1992
assoc.prof.salary:   Average salary for associate professors at the university in 1992
asst.prof.salary:   Average salary for assistant professors at the university in 1992

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1096) of an [OpenML dataset](https://www.openml.org/d/1096). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1096/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1096/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1096/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

